# Number Guessing Game

This Python script implements a number guessing game where the player needs to guess a randomly generated number within a specified range.

## Features

- Allows the player to set the range for the random number.
- Offers different difficulty levels:
  - Easy: 150% more attempts
  - Medium: Normal attempts
  - Hard: 50% fewer attempts
  - Unlimited: Infinite attempts
- Tracks the number of attempts made by the player.
- Validates user input to ensure it falls within the specified range.

## Usage

1. Run the script.
2. Enter the right border for the random number range.
3. Choose the game difficulty:
   - (E)asy
   - (M)edium
   - (H)ard
   - (U)nlimited
4. Start guessing the number within the specified range.
5. After each guess, the game will inform whether the guess is too high or too low.
6. Keep guessing until you either find the correct number or run out of attempts.
7. If you guess correctly, the game will display the number of attempts made and ask if you want to play again.

## Functions

```python
define_difficulty(boarder, symb)
```

Determines the number of attempts based on the game difficulty chosen.

- `boarder`: The upper limit of the range.
- `symb`: Difficulty level ('E' for Easy, 'M' for Medium, 'H' for Hard, 'U' for Unlimited).

```python
restart_game()
```

Restarts the game by generating a new random number and resetting the attempt counter.

```python
is_valid(num, right)
```

Checks if the input number is within the specified range.

- `num`: Input number.
- `right`: Right border of the range.
