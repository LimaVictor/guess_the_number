import random

def define_difficulty(boarder, symb):
    attempts = 0
    while 2 ** attempts < boarder:
        attempts += 1
    if symb == 'E': # 150%
        attempts += attempts // 2
        return attempts 
    elif symb == 'M': # 100%
        return attempts
    elif symb == 'H': # 50%
        attempts -= attempts // 2
        return attempts
    elif symb == 'U': # infinity
        attempts = float('inf')
        return attempts

def restart_game():
    number = random.randint(1, right_border)
    count = 0
    return number, count

def is_valid(num, right):
    if 1 <= num <= right:
        return True
    else:
        return False
    
print('Welcome to Number guessing game')
print('Please enter right border: ', end='')
right_border = int(input())
print('please select game difficulty [(E)asy, (M)edium, (H)ard or (U)nlimited]: ', end='')
difficulty = input().upper()
attempts_init = define_difficulty(right_border, difficulty)
shadow_number, try_counter = restart_game()
attempts_left = attempts_init

while (True):
    print(f'[{attempts_left} attempts left] Please enter your number: ', end='')
    input_number = int(input())
    try_counter += 1
    if is_valid(input_number, right_border):
        if input_number == shadow_number:
            print(f'You win in {try_counter} step')
            print('Do you want to play again? (Y)es/(N)o')
            if input().upper() == 'Y':
                shadow_number, try_counter = restart_game()
                attempts_left = attempts_init
            else:
                break
        elif input_number > shadow_number:
            print('Your number is higher, try again')
            attempts_left -= 1
        elif input_number < shadow_number:
            print('Your number is less, try again')
            attempts_left -= 1
    else:
        print('Invalid input: Number is out of range')
        continue
        
